const esModules = [
  // 'vuetify',
  // 'vuetify/components',
  // 'vuetify/directives',
  'ol',
  'monaco-editor',
  '@farmos.org',
  '@our-sci/farmos-map',
  'flat',
].join('|');

module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  transformIgnorePatterns: [`/node_modules/(?!${esModules})`],
  moduleNameMapper: {
    // 'monaco-editor': 'monaco-editor/esm/vs/editor/editor.api.js',
    // '^!!raw-loader!.*': 'jest-raw-loader',
    '^vuetify/components$': '<rootDir>/node_modules/vuetify/lib/components/index.mjs', //TODO desperate attempt without effect
    '^vuetify/directives$': '<rootDir>/node_modules/vuetify/lib/directives/index.mjs', //TODO desperate attempt without effect
    // '^@/(.*)$': '<rootDir>/src/$1', //TODO desperate attempt based on https://dev.to/imomaliev/creating-vite-vue-ts-template-setup-jest-5h1i, without effect
  },
}
